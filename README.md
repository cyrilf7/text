<h1 align="center">
  Text
</h1>
<p align="center"><small>Short texts and poems from the mind to the keyboard.</small></p>

Text is a personal project from [cyrilf](https://cyrilf.com)  
I use this website to store and display personal texts/poems/words.

<br/>

I'm mainly using [unsplash](https://unsplash.com) for the post images and also [undraw](https://undraw.co) for the svg image.

## Development details

Technically it's based on [Gatsby](https://gatsbyjs.org) + [Tailwind](https://tailwindcss.com) + [Forestry](https://forestry.io).

> Notes

This project doesn't use the `siteMetadata` section from `gatsby-config.js`. This is because I needed it to be configurable via CMS.
TO achieve this, the values need to be extracted into a JSON file.
The issue was that the `require` statement for this json file was breaking the live reload.
So, as a workaround, I decided to use gatsby-transform-json instead to handle this issue.
[See more about this issue here](https://github.com/gatsbyjs/gatsby/issues/21938).

## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a this project.

    .
    ├── .forestry
    |   ├── front_matter/templates
    ├── content
    ├── src
    ├── gatsby-config.js
    └── gatsby-node.js

1.  **`.forestry`**: This directory contains all of the configuration for the CMS forestry. This is totally optional and you can remove it if you're using a different CMS.

        It contains a `settings.yml` file for the all of forestry configuration (sidebar, deployment, options, ...)

    The subdirectory `front_matter/templates` contains all the configuration for the front_matter section of the md files.

2.  **`/content`**: This folder contains all the content editable by the editor (via the CMS). For instance, the posts, the pages and also the site configuration.

3.  **`/src`**: All of the dev content.

4.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby site. This is where you can specify information which Gatsby plugins you’d like to include, etc. (Check out the [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more detail).

5.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any). These allow customization/extension of default Gatsby settings affecting pieces of the site build process.

## 💫 Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/cyrilf7/text)

[![Deploy with ZEIT Now](https://zeit.co/button)](https://zeit.co/import/project?template=https://gitlab.com/cyrilf7/text)
