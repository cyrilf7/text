---
title: About
template: page
---

![about](/uploads/about.svg)

<br/>
<br/>
<br/>

Text is a personal project from [cyrilf](https://cyrilf.com)
I use this website to store and display personal texts/poems/words.

<br/>

I'm mainly using [unsplash](https://unsplash.com) for the post images and also [undraw](https://undraw.co) for the svg image.
Technically it's based on [Gatsby](https://gatsbyjs.org) + [Tailwind](https://tailwindcss.com) + [Forestry](https://forestry.io).
