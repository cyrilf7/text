---
title: "Alive"
date: 2016-01-25T00:00:00+01:00
excerpt: ""
image: "/uploads/alive.jpg"
imageAlt: "alive"
template: post
---

People who run  
People who smile  
People who shine  
People who laugh  
People who don't care  
People who dream  
People who make their dreams come true  
People who live  
Yeah, definitely, people who live.

I want to be a living person, I want to be alive!  
I want to live!  
Leave and live!
