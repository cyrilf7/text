---
title: "Anawhata"
date: 2016-01-23T00:00:00+01:00
excerpt: ""
image: "/uploads/anawhata.jpg"
imageAlt: "anawhata beach"
template: post
---

There is people that come into your life just like that, out of nowhere.  
Unexpected.  
These specific persons have an influence on your life, your vision, your choices that can't be measured.  
Usually they managed to plant a seed into your head. This little seed, guiltless and feeble will become quickly a strong idea residing in the depth of your mind.  
And that's only from there that you start measuring the power of this person.  
You didn't ask anything, but they open your mind one step ahead of where you were.  
The last time I met that kind of person, she opened my eyes and let me see that life has to be enjoyed purely and that the current moment is everything.  
Stop planning, throw away your ties and engagements, just live, now!  
Take a deep breathe, look around you, isn't it good to feel alive?

Stand up, go outside, walk and aim for what make you happy!  
For me, it was going to the other side of the world, in New Zealand, leaving all behind and just a dream in front of me.  
Guess what, I'm now leaving in my dream. Everyday. Every second.  
By all means, avoid any regrets. Do what you like and do it now!  
Make your dream come true, you're awesome, just do it!
