---
title: Butterfly
date: 2016-02-13T00:00:00+01:00
excerpt: ""
image: "/uploads/butterfly.jpg"
imageAlt: "girl running in sunny field"
template: post
---

You're free as the wind, so I'll never catch you.  
Even if I run, faster you'll do.

Always having a leap ahead,  
I'm no longer in the game.

This butterfly flying is so poetic,  
but the end of this story sounds quite tragic.

I'll miss you if our paths don't cross,  
but still I wish we won't live that loss.

The road is open, there is a lot to come,  
and I'm already dreaming of the outcome.
