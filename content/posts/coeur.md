---
title: "Coeur"
date: 2015-09-01T00:00:00+01:00
excerpt: ""
image: "/uploads/coeur.jpg"
imageAlt: "coeur"
template: post
---

Jours et nuits je pense à cela, mais je pense que c'est elle que j'ai au fond de moi.  
Tu sais dans le coeur.  
Je lui prend la main, on court, on se perd, on perd le Nord.  
Mais pas l'amour, l'amour lui dure encore.

<br/>

Encore et toujours qu'importe les directions.  
Qu'importe les vents et les décisions, sages ou brutales, il n'y a pas de bons choix.  
Il n'y a que des bons choix et je ne sais plus ce que je dis.  
Je perds la tête mais pas le Nord.

<br/>

C'est cette fille qui m'aiguille et même sans boussole je trouve mon chemin.  
Enfin notre chemin, car c'est celui qui me mène vers elle et qui nous permet de se prendre la main pour marcher doucement, car après la nuit vient le jour.  
Et c'est comme ça tous les jours, tu sais, dans mon coeur.
