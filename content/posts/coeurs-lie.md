---
title: "Cœurs liés"
date: 2015-10-13T00:00:00+01:00
excerpt: ""
image: "/uploads/coeurs-lie.jpg"
imageAlt: "couple"
template: post
---

Deux cœurs si proches renforcés par les sentiments qui les lies.  
L'amour les rapproche et le réchauffement fait fondre l'étain qui les étreints encore plus.  
Resseré par l'étau sentimental aucun ne cherche à se détacher.  
Se briser ? Oh non, jamais.  
Trop proches, trop forts. Mais une absence d'efforts peut rendre la partition illisible.  
La musique de leur sentiments s'éteint peu à peu et seul le temps sera désserrer leur embrasement.
