---
title: Free
date: 2016-02-12T00:00:00+01:00
excerpt: ""
image: "/uploads/free.jpg"
imageAlt: "urbex"
template: post
---

Nothing is unreachable if you give yourself the means. Climb up, climb up if you want to. Why shouldn't it be possible? There is no law, no rule that should restrain you.  
You want to be free? You already are. Just listen to your heart. Stop thinking.  
Be yourself and start living.
