---
title: "Friends"
date: 2015-12-12T00:00:00+01:00
excerpt: ""
image: "/uploads/friends.jpg"
imageAlt: "friends"
template: post
---

You, fucking friends.  
I'm writing this as drunk as I can.  
I'm still consious but I just want to tell,  
how much I love you but I'll never tell.

<br/>

You bring me more that I will ever do.  
So I just want to say thank you.  
I wish more than everything that you came with me.  
The world is huge but far away I'll be.

<br/>

I'm not doing any poem of that kind of stuff.  
Cause did you see any verse in the last words?

<br/>

Well anyway I wish you all the best, you're awesome and you can't deserve less.  
I'll repeat myself but I love you, I swear I do.

See you in a year or maybe two.  
The life is big and so your dreams need too.  
Enjoy your life, smile anytime, I already miss you. We're the 12 of December, so there still a lot to live.  
Sorry for the honesty..  
A speaking man on his subway, direction, his city.  
Don't ever give up on your dreams and dream big!
