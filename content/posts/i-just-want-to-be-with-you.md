---
title: "I just want to be with you"
date: 2016-03-07T00:00:00+01:00
excerpt: ""
image: "/uploads/with-you.jpg"
imageAlt: "with you"
template: post
---

I just want to be with you.  
Ain't got nothing more simple than that.  
Just being able to feel you, see you there, smiling.  
Look at the sun and dream together, here, in the grass.  
Look at a butterfly and laugh or just blow a dandelion.  
The more basic things are the best.  
I want to feel the nature surrounding us and I want to feel you.  
Our minds only focused on the present moment, because it's the only one that matters, the only one that exists.  
Free of everything but the feelings, the joy and the happiness.  
Is that all a dream? Am I asking too much?  
I don't think so, but time will bring us the answer.  
In the meantime I keep smiling, I keep dreaming and I keep embracing life.  
Can you see the stars? Can you see the wind blowing the leaves of that tree?  
The bird, here, the bird is flying, up, up, he is free.  
As much as we are, fly, little butterfly, fly.  
Dance in the sky and breathe the world.  
Open your wings and feel alive.  
One day, we'll meet together, and that day will feel like a big-bang. Not in a bad way, only an unstoppable explosion of raw energy. It's pretty rare to live those moments so will need to take care of it and keep the firework going on and on. The lights will shine and the color will be vivid.  
Oh I promise, what a beauty, the best piece of art you'll ever experience.  
But, enough talking, the dream is not done yet.  
I just want to be with you.
