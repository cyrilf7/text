---
title: Lumière
date: 2016-01-07T00:00:00+01:00
excerpt: ""
image: "/uploads/lumiere.jpg"
imageAlt: "rainy city lights"
template: post
---

Ça brille, ça brille !  
Au revoir Paris, putain de ville lumière.  
Et des lumières, je peux l'avouer, j'en ai croisé.  
Pendant que ce métro file et que les rues défilent j'ai le temps de me remémorer tous les souvenirs que j'ai pu accumuler. Ils sont forts, ils sont beaux, ils sont intenses. Ouvertement, ils me touchent.  
La pluie sur les pavets, ça scintille, ça brille.  
De la lumière, toujours plus, plus, en cette matinée sombre.  
Un départ n'est jamais simple, tourner la page non plus, mais c'est la tête haute et le coeur lourd que je dis au revoir à cette aventure.  
À bientôt vous tous, sur les toits, en dessous ou dans la rue. Le manque est là et il est fort.  
Gardons le sourire et la lumière de nos cœurs.
