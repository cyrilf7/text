---
title: Spark
date: 2016-02-06T00:00:00+01:00
excerpt: ""
image: "/uploads/spark.jpg"
imageAlt: "girl in the sun"
template: post
---

This girl is like the morning light.  
Intense and pleasant.  
Her hairs shine and are wild enough to put a spell on you.  
You can't avoid it and that's for the best.  
You embrace the raw emotion.  
And feel good doing so!  
But did I tell you about her eyes?  
No, I don't, I won't.  
I mean, can I?  
Surely not.  
So let's talk about her smile then.  
Well, to be honest, won't be easier.  
Majestic and purely enchanting.  
Bewitched.  
So what now?  
I rather disappear to be safe and sound.  
See you later, I mean, for real.  
No one can live without hard drugs.  
I found one, you.
