---
title: "Storyteller"
date: 2016-07-18T00:00:00+01:00
excerpt: ""
image: "/uploads/storyteller.jpg"
imageAlt: "storyteller"
template: post
---

She's a storyteller but she doesn't use words to express herself.  
Instead, she uses drawings and pretty good ones to be honest.  
Real pieces of art. And when I say art, I mean gold.  
Something so pure that you don't want it to be sold.

<br/>

I know art is different to anyone  
And I'm usually quite picky.  
But her is raw, powerful, vibrant  
And also full of honesty.

<br/>

It implies wilderness, feels natural and connect with your roots.  
I swear if you saw it you won't have any doubts.  
She's mastering it and that's a real pleasure to see.  
She's a creative, yes, she's an artist believe me.

<br/>

Draw keep drawing. You'll go far.  
And as the last sentence I'll choose: "Paris, c'est de l'art"
