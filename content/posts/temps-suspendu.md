---
template: post
title: Temps suspendu
date: 2020-09-15T00:00:00+02:00
excerpt: ''
image: "/uploads/aron-visuals-bxoxnq26b7o-unsplash-1.jpg"
imageAlt: sablier

---
J'ai l'impression que le temps est suspendu quand tu n'es pas là.  
Quand nous reverrons nous la prochaine fois ? Et est-ce qu'on se reverra ?

Mon sablier ne s'écoule plus, le sable est figé.  
Le sentiment amer d'être envoûté.  
Et sans antidote pour ce maléfice,  
Me voilà bloqué, fixe.

Les palpitations de mon cœur marquent le tempo.  
Mais à quoi bon si l'aiguille des secondes affiche zéro.  
On avait le temps, mais je ne l'ai plus.  
Je croyais pourtant que tu me l'aurais rendu.

Comment ça se passe de ton côté ? Ça va deux fois plus vite ou ça s'est arrêté ?

J'ai tellement envie de changer le cours des choses.  
Ne pas retomber dans un quotidien morose.  
Mais heureusement la bonne nouvelle,  
C'est que si le temps n'est plus,  
Seul le moment présent est réel.  
Et que si il ne me convient pas,  
Libre à moi de le changer.  
Libre à moi de te retrouver.