---
title: "This voice"
date: 2016-06-10T00:00:00+01:00
excerpt: ""
image: "/uploads/this-voice.jpg"
imageAlt: "this voice"
template: post
---

Who's talking?

I know the tone of that voice. It strangely sounds very familiar.  
What's weird though is that I'm the only one able to hear it.  
Maybe it's because I'm the only one who pay attention and the others just don't mind.  
I think they should because this familiar voice is pretty smart. Well it can be dumb at times too, I admit it.  
My only concern is that it's pretty hard for it to be silent. Even when I ask it to shut.

Anyway, I think I'm used to it now.  
Someday I share good ideas and thoughts with it, but from time to time we're just the worst enemies.  
In the end, I like it, I respect it.

Now, I realize that this voice is familiar for one reason. It's the exact same voice than mine, the one you can hear, expect that this one stay in my head.

Oh wait, it seems like I didn't write this text. It did. This voice dictated every words and I just pressed the keys.

There is so much confusion going on about who's who. Some kind of conflicts by these last words about who's ideas are this really from. Am I really myself or this voice is, me?

Hard to say.

And you, who read this text? Yourself or is it your inner voice reading it right now. From here I swear that I can hear your own voice reading it, in your head.

Ok, it's time to shut it down for a bit in order to make things clearer.

But wait, I can't, you neither.
