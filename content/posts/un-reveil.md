---
template: post
title: Un réveil
date: 2020-09-19T00:00:00+02:00
excerpt: ''
image: "/uploads/kalegin-michail-ffustacax0e-unsplash-1.jpg"
imageAlt: reveil train

---
Comme un réveil non voulu.  
Il nous fait perdre le sommeil, il nous éveille, il nous réveille.  
On perd la veille; le jour d'avant.  
Nous voilà dans le jour d'après, l'aujourd'hui, le maintenant.  
Tenant à nous rappeler que le temps n'est qu'illusion.  
Seul le moment présent compte et existe.  
Le passé, oublié. Le futur, pas encore prêt.

<br/>  
Non, tout ce qu'on aura jamais est là. Ici et maintenant.  
Ne cherche pas, ne cherche plus, tu t'es trouvé.  
Un mélange intense et condensé de sons, de sensations, de ressentis que tu éprouves; là.  
Ce point infiniment petit, insaisissable qu'est le moment présent. Pourtant il dure, il dure, indéfiniment. Rien ne l'arrête.  
Et dans quel but ? Mettre en pause la vie ?  
Impossible, la vie est indomptable et bien trop sauvage.  
Elle est faite pour s'échapper, pour jouir, pour succomber de trop. Ou de pas assez. Mais on ne l'arrête pas, non, jamais.  
Inspire. Sens la vie entrer en toi.  
Vie, maintenant.

<br/>  
Jette les menottes et détache les liens qui te retiennent. Brûle les règles et soi toi même. Réveil ton enfant intérieur.e, laisse l.e.a s'exprimer, sans contrainte ni préjugé.  
Court cri chante si ça te plaît.  
On s'en fou. Goûte à la liberté et savoure. Extasie toi.  
J'aime te voir comme ça.  
J'aime.

<br/>  
La mauvaise nouvelle, là voilà.  
C'est pas voulu, mais le réveil sonne et sonnera encore.  
En somme c'est la fin de ce texte. Beau prétexte pour arrêter les mots et laisser la nuit se dissiper. Un nouveau soleil se lève, à l'ouest, autant que moi.  
Que ce jour soit beau. Je te laisse.