---
title: "Updside down"
date: 2016-06-03T00:00:00+01:00
excerpt: ""
image: "/uploads/upside-down.jpg"
imageAlt: "upside down"
template: post
---

The sea is here and the waves are singing.  
The light breeze come and go gently.  
It's a beauty to see the sun light fill the landscape.  
I can feel her next to me and that's when it all makes sense. The world change for the better, I let it go.

<br/>

I let her go and everything's upside down.  
It's a non sense that I can't see her.  
The wind is strong and definitely not friendly.  
The silence start to impose himself and the shore is gradually disappearing.
