const path = require("path")
const { createFilePath } = require("gatsby-source-filesystem")
const { fmImagesToRelative } = require("gatsby-remark-relative-images")

const getPreviousNextPosts = (posts, index) => {
  const previousPost = index === posts.length - 1 ? null : posts[index + 1].node
  const nextPost = index === 0 ? null : posts[index - 1].node

  const previous = previousPost
    ? { slug: previousPost.fields.slug, title: previousPost.frontmatter.title }
    : null
  const next = nextPost
    ? { slug: nextPost.fields.slug, title: nextPost.frontmatter.title }
    : null

  return [previous, next]
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const result = await graphql(
    `
      {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                title
                template
              }
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  // Create a page for each posts/page.
  const postsAndPages = result.data.allMarkdownRemark.edges
  const pageComponent = path.resolve("./src/templates/page.js")
  const postComponent = path.resolve("./src/templates/post.js")

  const pages = []
  const posts = []
  postsAndPages.forEach(p =>
    p.node.frontmatter.template === "page" ? pages.push(p) : posts.push(p)
  )

  posts.forEach((post, index) => {
    const [previous, next] = getPreviousNextPosts(posts, index)

    createPage({
      path: post.node.fields.slug,
      component: postComponent,
      context: {
        slug: post.node.fields.slug,
        previous,
        next
      }
    })
  })

  pages.forEach(post => {
    createPage({
      path: post.node.fields.slug,
      component: pageComponent,
      context: {
        slug: post.node.fields.slug
      }
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  fmImagesToRelative(node)

  if (node.internal.type === "MarkdownRemark") {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: "slug",
      node,
      value
    })
  }
}
