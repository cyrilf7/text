module.exports = {
  variants: {
    backgroundColor: ["responsive", "group-hover", "hover", "active"],
    borderColor: ["responsive", "group-hover", "hover", "active"],
    scale: ["group-hover", "hover"],
    opacity: ["group-hover", "hover"]
  }
}
